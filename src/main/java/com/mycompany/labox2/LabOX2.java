/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */
package com.mycompany.labox2;

import java.util.Scanner;

/**
 *
 * @author Murphy
 */
public class LabOX2 {

    static char[][] table = {{'-', '-', '-'}, {'-', '-', '-'}, {'-', '-', '-'}};
    static char currentPlayer = 'O';
    static int row, col;
    static int countRound = 0;
    static char playAgain = '-';

    public static void main(String[] args) {

        while (true) {

            printWelcome();
            while (true) {

                if (noPlayerWin()) {
                    printTable();
                    printDraw();
                    break;
                }

                printTable();
                printPlayerTurn();
                inputRowCol();

                if (currentPlayerWin()) {
                    printTable();
                    printWinner();
                    break;
                }
                countRound = countRound + 1;
                changePlayer();
            }

            if (countRound > 0) {
                printWantPlayAgain();
                inputPlayAgain();
            }
            if (playAgain == 'N') {
                break;
            }

            resetStat();

        }

    }

    private static void printWelcome() {
        System.out.println("welcome to OX");
    }

    private static void printTable() {
        System.out.println("-------------------");
        for (int i = 0; i < table.length; i++) {
            for (int j = 0; j < table.length; j++) {
                System.out.print("| " + table[i][j] + " ");
            }
            System.out.print("|");
            System.out.println("");
        }
        System.out.println("-------------------");
    }

    private static void printPlayerTurn() {
        System.out.print("Player " + currentPlayer + ",");
    }

    private static void inputRowCol() {
        Scanner kb = new Scanner(System.in);
        System.out.print(" enter your row, col: ");
        while (true) {
            row = kb.nextInt();
            col = kb.nextInt();
            if (table[row - 1][col - 1] == '-') {
                table[row - 1][col - 1] = currentPlayer;
                return;
            } else {
                System.out.print("Player " + currentPlayer + ", enter your row, col: ");
            }
        }

    }

    private static void changePlayer() {
        if (currentPlayer == 'O') {
            currentPlayer = 'X';
        } else {
            currentPlayer = 'O';
        }
    }

    private static void printWinner() {
        System.out.println("Player " + currentPlayer + " wins!");
    }

    private static boolean currentPlayerWin() {
        if (checkRow()) {
            return true;
        } else {
            if (checkCol()) {
                return true;
            } else {
                if (checkTopLeft_to_BottomRight()) {
                    return true;
                } else {
                    if (checkTopRight_to_BottomLeft()) {
                        return true;
                    }
                }
            }
        }
        return false;
    }

    private static boolean checkRow() {
        for (int i = 0; i < table.length; i++) {
            if (table[row - 1][i] != currentPlayer) {
                return false;
            }
        }
        return true;
    }

    private static boolean checkCol() {
        for (int i = 0; i < table.length; i++) {
            if (table[i][col - 1] != currentPlayer) {
                return false;
            }
        }
        return true;
    }

    private static boolean checkTopLeft_to_BottomRight() {
        if (table[0][0] != currentPlayer) {
            return false;
        } else {
            if (table[1][1] != currentPlayer) {
                return false;
            } else {
                if (table[2][2] != currentPlayer) {
                    return false;
                }
            }
        }
        return true;
    }

    private static boolean checkTopRight_to_BottomLeft() {
        if (table[0][2] != currentPlayer) {
            return false;
        } else {
            if (table[1][1] != currentPlayer) {
                return false;
            } else {
                if (table[2][0] != currentPlayer) {
                    return false;
                }
            }
        }
        return true;
    }

    private static boolean noPlayerWin() {
        if (countRound >= 9) {
            return true;
        }
        return false;
    }

    private static void printDraw() {
        System.out.println(">> Draw No Winner <<");
    }

    private static void printWantPlayAgain() {
        System.out.print("Do you want to play again?(Y/N): ");
    }

    private static void inputPlayAgain() {
        Scanner sc = new Scanner(System.in);
        playAgain = sc.next().charAt(0);
    }

    private static void resetStat() {
        for (int i = 0; i < table.length; i++) {
            for (int j = 0; j < table.length; j++) {
                table[i][j] = '-';
            }
        }
        currentPlayer = 'O';
        countRound = 0;
    }
}
